# Nearcut Coding Challenge

This is a repository for Nearcut Coding Challenge

Background:

* A CSV file upload containing users name and password is uploaded from the home page, and the result of the users import is displayed.

* A user record which doesn't meet the "strong" password requirement will not be saved, and the required number of changes will be displayed.

* A custom PasswordValidator is used to check for "strong" password requirements.


