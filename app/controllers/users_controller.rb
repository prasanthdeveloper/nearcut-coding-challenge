class UsersController < ApplicationController
  def create
    @results = ::CsvImporter.new(params[:file]).import!
    respond_to do |format|
      format.js
    end
  end
end
