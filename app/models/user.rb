class User < ActiveRecord::Base
  attr_accessible :name, :password

  validates :name, presence: true
  validates_with PasswordValidator

  class << self
    def create_from_csv!(name:, password:)
      user = User.create!(name: name, password: password)
      "#{user.name} was successfully created"
    rescue ActiveRecord::RecordInvalid => invalid
      invalid.record.errors.full_messages.first
    end
  end
end
