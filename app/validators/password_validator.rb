# class responsible for validating user password
class PasswordValidator < ActiveModel::Validator
  MINIMUM_PASSWORD_LENGTH = 10
  MAXIMUM_PASSWORD_LENGTH = 16

  def validate(record)
    password = record.password
    record.errors.add(:password, "can't be blank") unless password.present?
    return if record.errors.present?

    password_changes_count = 0

    %i[password_length_changes password_case_and_digit_changes repeating_chars_changes].each do |logic_method|
      password_changes_count += self.send(logic_method, password)
      break if password_changes_count > 0
    end
    record.errors.add(:base, "Change #{password_changes_count} #{'character'.pluralize(password_changes_count)} of #{record.name}'s password") if password_changes_count > 0
  end

  private

  def password_length_changes(password)
    password_size = password.size
    if password_size < MINIMUM_PASSWORD_LENGTH
      MINIMUM_PASSWORD_LENGTH - password_size
    elsif password_size > MAXIMUM_PASSWORD_LENGTH
      password_size - MAXIMUM_PASSWORD_LENGTH
    else
      0
    end
  end

  def password_case_and_digit_changes(password)
    case_and_digit = []
    case_and_digit << password[/[A-Z]/]
    case_and_digit << password[/[a-z]/]
    case_and_digit << password[/\d/]
    3 - case_and_digit.compact.size
  end

  def repeating_chars_changes(password)
    password.scan(/(.)\1\1/).count
  end
end
