# Load the rails application
require File.expand_path('../application', __FILE__)
require File.expand_path('../initializers/postgresql_adapter_monkeypatch', __FILE__)

# Initialize the rails application
NearcutCodingChallenge::Application.initialize!
