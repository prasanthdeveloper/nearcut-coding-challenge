require 'csv'

class ::CsvImporter
  ALLOWED_FILE_CONTENT_TYPES = ['text/csv']

  attr_reader :file

  def initialize(file)
    @file = file
  end

  def import!
    if valid_file_uploaded?
      results = []
      file_path = file.path
      csv_text = File.read(file_path).delete!("\r")
      CSV.parse(csv_text, headers: true, col_sep: ',').each do |row|
        results << User.create_from_csv!(name: row['name'], password: row['password'])
      end
      results
    else
      'Please upload a valid CSV file'
    end
  end

  private

  def valid_file_uploaded?
    file.present? && ALLOWED_FILE_CONTENT_TYPES.include?(file.content_type)
  end
end
