require 'rails_helper'

describe UsersController do
  render_views

  describe 'POST :create JS' do
    subject { post :create, { format: 'js', 'file' => Rack::Test::UploadedFile.new(fixture_path, fixture_content_type) } }

    context 'when a valid CSV is uploaded' do
      let(:fixture_path) { Rails.root.join('spec', 'fixtures', 'users.csv') }
      let(:fixture_content_type) { 'text/csv' }

      it 'parses csv and returns response successfully' do
        expect { subject } .to change { User.count }.from(0).to(1)
        expect(subject).to be_success
        expect(subject.body).not_to include('Please upload a valid CSV file')
      end
    end

    context 'but a non-CSV file is uploaded' do
      let(:fixture_path) { Rails.root.join('spec', 'fixtures', 'users.html') }
      let(:fixture_content_type) { 'text/html' }

      it 'returns an error message' do
        expect { subject }.not_to change { User.count }
        expect(subject.body).to include('Please upload a valid CSV file')
      end
    end
  end
end
