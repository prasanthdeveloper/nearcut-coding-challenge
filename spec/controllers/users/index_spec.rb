require 'rails_helper'

describe UsersController do
  render_views

  describe 'GET :index' do
    subject { get :index }

    it 'renders successfully' do
      expect(subject).to be_success
      expect(subject).to render_template('users/index')
    end
  end
end
