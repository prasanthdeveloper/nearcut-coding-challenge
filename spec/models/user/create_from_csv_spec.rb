require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new(name: user_name, password: user_password) }
  let(:user_name)     { 'Valid User Name' }
  let(:user_password) { 'ABc1234JJksL' }

  describe '#create_from_csv!' do
    subject { described_class.create_from_csv!(name: user_name, password: user_password) }

    context 'when user name and password are valid' do
      it 'returns users successfully created message' do
        expect(subject).to eql("#{user_name} was successfully created")
      end
    end

    context 'but user name is not present' do
      let(:user_name)     { }

      it 'validation error message' do
        expect(subject).to eql("Name can't be blank")
      end
    end

    context 'but user password is not present' do
      let(:user_password)     { }

      it 'validation error message' do
        expect(subject).to eql("Password can't be blank")
      end
    end

    context 'but user password is invalid' do
      let(:user_password)     { 'KL123' }

      it 'validation error message' do
        expect(subject).to eql("Change 5 characters of #{user_name}'s password")
      end
    end
  end
end
