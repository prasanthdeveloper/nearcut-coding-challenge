require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new(name: user_name, password: user_password) }
  let(:user_name) { 'Valid User Name' }
  let(:user_password) { 'Aqpfk1swods' }
  let(:validator) { PasswordValidator.new({}) }

  describe 'validation error' do
    context 'when validating name' do
      context 'and name is present' do
        it 'does not raise any error' do
          expect(user).to be_valid
          expect(user.errors.full_messages).to be_empty
        end
      end

      context 'but name is not present' do
        let(:user_name) { }

        it 'raises error' do
          expect(user).not_to be_valid
          expect(user.errors.full_messages).to include("Name can't be blank")
        end
      end
    end

    context 'when validating password' do
      before do
        validator.validate(user)
      end

      context 'and user password is valid' do
        it 'does not raise any error' do
          expect(user).to be_valid
          expect(user.errors.full_messages).to be_empty
        end
      end

      context 'but user password is not present' do
        let(:user_password) { }

        it 'raises error' do
          expect(user).not_to be_valid
          expect(user.errors.full_messages).to include("Password can't be blank")
        end
      end

      [
        { given: 'AaB9K', expectation: 5 },
        { given: '@#9KLrt', expectation: 3 },
        { given: 'oityrH90L', expectation: 1 }
      ].each do |test_options|
        context "but user password is shorter than 10 characters like #{test_options[:given]}" do
          let(:user_password) { test_options[:given] }

          it 'raises error and list the number of changes required' do
            expect(user).not_to be_valid
            expect(user.errors.full_messages).to include("Change #{test_options[:expectation]} #{'character'.pluralize(test_options[:expectation])} of #{user.name}'s password")
          end
        end
      end

      [
        { given: 'AaB9KAaB9KAaB9K123', expectation: 2 },
        { given: '123456789oilerhtgBN@', expectation: 4 },
        { given: 'polIYTjh764393HTr', expectation: 1 }
      ].each do |test_options|
        context "but user password is longer than 16 characters like #{test_options[:given]}" do
          let(:user_password) { test_options[:given] }

          it 'raises error and list the number of changes required' do
            expect(user).not_to be_valid
            expect(user.errors.full_messages).to include("Change #{test_options[:expectation]} #{'character'.pluralize(test_options[:expectation])} of #{user.name}'s password")
          end
        end
      end

      [
        { given: 'TYUIOPLKJ9Y', expectation: 1 },
        { given: 'lkjhgtmrn90', expectation: 1 },
        { given: 'tyrewqshdfj', expectation: 2 },
        { given: 'OLKMNHJYTRF', expectation: 2 },
        { given: '12345678908', expectation: 2 },
        { given: '!@#$%^&*()&', expectation: 3 }
      ].each do |test_options|
        context "but user password does not have at least one lowercase, uppercase or digit like #{test_options[:given]}" do
          let(:user_password) { test_options[:given] }

          it 'raises error and list the number of changes required' do
            expect(user).not_to be_valid
            expect(user.errors.full_messages).to include("Change #{test_options[:expectation]} #{'character'.pluralize(test_options[:expectation])} of #{user.name}'s password")
          end
        end
      end

      [
        { given: 'AAAfk1swods', expectation: 1 },
        { given: '000aaaBBBccccDDD', expectation: 5 },
        { given: '000aaaBBBccCcDDD', expectation: 4 },
        { given: '000aaaBBBc.ccDDD', expectation: 4 }
      ].each do |test_options|
        context "but user password has blocks of repeating characters like #{test_options[:given]}" do
          let(:user_password) { test_options[:given] }

          it 'raises error and list the number of changes required' do
            expect(user).not_to be_valid
            expect(user.errors.full_messages).to include("Change #{test_options[:expectation]} #{'character'.pluralize(test_options[:expectation])} of #{user.name}'s password")
          end
        end
      end

      [
        { given: 'ABC123', expectation: 4 },
        { given: 'KKKSS123', expectation: 2 },
        { given: '123456789123456789', expectation: 2 },
        { given: 'BBBccCDDD', expectation: 1 }
      ].each do |test_options|
        context "but user password has a combination of errors like #{test_options[:given]}" do
          let(:user_password) { test_options[:given] }

          it 'raises error and list the number of changes required' do
            expect(user).not_to be_valid
            expect(user.errors.full_messages).to include("Change #{test_options[:expectation]} #{'character'.pluralize(test_options[:expectation])} of #{user.name}'s password")
          end
        end
      end
    end
  end
end
