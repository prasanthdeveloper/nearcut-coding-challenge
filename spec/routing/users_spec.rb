require 'rails_helper'

describe UsersController do
  it 'allows selected routes' do
    expect(get('/users')).to route_to(controller: 'users', action: 'index')
    expect(post('/users')).to route_to(controller: 'users', action: 'create')
  end

  it 'rejects unsupported routes' do
    expect(get('/users/new')).to_not be_routable
    expect(put('/users/1')).to_not be_routable
    expect(delete('/users/1')).to_not be_routable
    expect(get('/users/1/edit')).to_not be_routable
  end
end
